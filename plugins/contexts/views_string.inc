<?php

/**
 * @file
 *
 * Plugin to provide a string context
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Views data as string'),
  'description' => t('A context that contains data from views'),
  'context' => 'vdc_create_view_string',
  'edit form' => 'vdc_settings_form',
  'keyword' => 'views_data',
  'context name' => 'views_data',

  'convert list' => array(
    'raw' => t('Views raw value'),
    'rendered' => t('Views rendered value'),
  ),
  'convert' => 'vdc_context_convert',
  'convert default' => 'raw',
);

/**
 * Convert a context into a string.
 */
function vdc_context_convert($context, $type) {
  return $type;
}

function vdc_create_view_string($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('views_data');
  $context->plugin = 'views_data';

  if ($empty) {
    return $context;
  }

  if ($data !== FALSE) {
    if ($conf) {
      $views_settings = explode('|', $data['view_display']);
      $view_name = $views_settings[0];
      $display_name = $views_settings[1];

      // Create an array of arguments.
      if (empty($data['view_arguments'])) {
        $data['view_arguments'] = '';
      }
      $view_arguments = explode("\n", $data['view_arguments']);
      $view_arguments = array_map('trim', $view_arguments);
      $view_arguments = array_filter($view_arguments, 'strlen');

      // Load the view and set the properties.
      $view = views_get_view($view_name);
      $view->set_display($display_name);
      $view->set_arguments($view_arguments);
      $view->build();

      // Get first field name in view
      $field_names = array_keys($view->field);
      $field = $field_names[0];
      $view->execute($view->current_display);
      if (!empty($view->result[0])) {
        if (!empty($view->result[0]->{'field_' . $field}[0]['rendered']['#markup'])) {
          $_data = $view->result[0]->{'field_' . $field}[0]['rendered']['#markup'];
          if (!empty($data['strip_tags'])) {
            $_data = strip_tags($_data);
          }
          if (!empty($data['trim_whitespaces'])) {
            $_data = trim($_data);
          }
          $data['views_data'] = $_data;
        }
        else {
          $data['views_data'] = '';
        }
      }

      // Process if value is empty
      if (empty($data['views_data'])) {
        switch ($data['empty_behaviour']) {
          case 'empty_string':
            $data['views_data'] = '';
            break;
          case 'view_field_option':
            $data['views_data'] = $view->field[$field]->options['empty'];
            break;
          case 'input_value':
            $data['views_data'] = $data['empty_value'];
            break;
        }
      }
    }

    $context->data = $data;
    $context->title = ($conf) ? check_plain($data['views_data']) : check_plain($data['identifier']);
    return $context;
  }
}

function vdc_settings_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $displays = _vdc_views_list();

  $form['view_display'] = array(
    '#type' => 'select',
    '#title' => t('View and display'),
    '#description' => t('Select view display returning value. Data from first field/column of first result row will be used.'),
    '#options' => $displays,
    '#default_value' => !empty($conf['view_display']) ? $conf['view_display'] : array(),
  );
  $form['view_arguments'] = array(
    '#type' => 'textarea',
    '#title' => t('Arguments'),
    '#description' => t('Any arguments to pass to the view, one per line.'),
    '#default_value' => !empty($conf['view_arguments']) ? $conf['view_arguments'] : '',
  );
  $form['strip_tags'] = array(
    '#type' => 'checkbox',
    '#title' => t('Strip tags'),
    '#default_value' => !empty($conf['strip_tags']) ? $conf['strip_tags'] : 0,
  );
  $form['trim_whitespaces'] = array(
    '#type' => 'checkbox',
    '#title' => t('Trim whitespaces'),
    '#default_value' => !empty($conf['trim_whitespaces']) ? $conf['trim_whitespaces'] : 0,
  );
  $form['empty_behaviour'] = array(
    '#type' => 'select',
    '#title' => t('Empty behaviour'),
    '#options' => array(
      'empty_string' => t('Empty string (do nothing)'),
      'view_field_option' => t('View field option value'),
      'input_value' => t('Input value'),
    ),
    '#default_value' => !empty($conf['empty_behaviour']) ? $conf['empty_behaviour'] : 'empty_string',
  );
  $form['empty_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Empty value'),
    '#states' => array(
      'visible' => array(
        ':input[name="empty_behaviour"]' => array('value' => 'input_value'),
      ),
    ),
  );

  return $form;
}

function vdc_settings_form_submit($form, &$form_state) {
  $fieldnames = array(
    'view_display',
    'view_arguments',
    'strip_tags',
    'trim_whitespaces',
    'empty_behaviour',
    'empty_value',
  );

  foreach ($fieldnames as $fieldname) {
    $form_state['conf'][$fieldname] = $form_state['values'][$fieldname];
  }
}

/**
 * Retrieve accessible views list
 * @return array
 */
function _vdc_views_list() {
  $selectable_displays = array();
  foreach (views_get_enabled_views() as $name => $view) {
    foreach ($view->display as $display_name => $display) {
      $view->build($display_name);
      if ($display->display_options['row_plugin'] == 'fields') {
        $selectable_displays[$view->name . '|' . $display_name] = check_plain($view->human_name) . ' | ' . check_plain($display->display_title);
      }
    }
  }
  return $selectable_displays;
}
